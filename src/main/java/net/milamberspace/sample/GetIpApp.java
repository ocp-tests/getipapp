package net.milamberspace.sample;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.net.whois.WhoisClient;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GetIpApp {

	private static final String VERSION = "37";

	private static final String PONG_RESPONSE = "Pong!";

	private String whoisSvr = WhoisClient.DEFAULT_HOST;

	private HttpServletRequest request;

	@RequestMapping("/")
	public String index() {
		return "Your IP address: [" + getClientIp() + "]";
	}

	@RequestMapping("/ping")
	public String ping() {
		return getPong();
	}

	@RequestMapping("/time")
	public String time() {
		return "Time is: " + getTime();
	}

	@RequestMapping("/date")
	public String date() {
		return "Time is: " + getDate();
	}

	@RequestMapping("/dateandtime")
	public String dateandtime() {
		return "The date and the time are: " + getDateAndTime();
	}

	@RequestMapping("/datetime")
	public String datetime() {
		return dateandtime();
	}

	@RequestMapping("/version")
	public String version() {
		return "The version is: " + getVersion();
	}

	@RequestMapping("/name")
	public String name() {
		return "Name: " + getName();
	}

	@RequestMapping("/all")
	public String all() {
		return getAll();
	}

	@RequestMapping("/whois/{domain}")
	public String whois(@PathVariable("domain") String domain) {
		return getWhois(domain);
	}

	@Autowired
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	private String getClientIp() {

		String remoteAddr = "";

		if (request != null) {
			remoteAddr = request.getHeader("X-FORWARDED-FOR");
			if (remoteAddr == null || "".equals(remoteAddr)) {
				remoteAddr = request.getRemoteAddr();
			}
		}

		return remoteAddr;
	}

	private String getTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		return dateFormat.format(date);
	}

	private String getDateAndTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

	static private String getVersion() {
		return VERSION;
	}

	static private String getPong() {
		return PONG_RESPONSE;
	}

	static private String getName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return "Error: unable to get the hostname";
	}

	private String getAll() {
		StringBuilder sb = new StringBuilder();
		sb.append("Date / Time: " + getDateAndTime() + "\n");
		sb.append("Remote IP: " + getClientIp() + "\n");
		sb.append("Node: " + getName() + "\n");
		sb.append("Version of getipapp: " + getVersion() + "\n");
		return sb.toString();
	}

	private String getWhois(String domain) {

		// System.out.println("Domain=" + domain);
		String country = new String();
		String regex = ".*\\.(\\w+)$";
		// System.out.println("regex=" + regex);
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(domain);
		if (matcher.find()) {
			country = matcher.group(1);

			StringBuilder findWhoisServer = new StringBuilder("");
			WhoisClient whois0 = new WhoisClient();
			try {

				whois0.connect("whois.iana.org");
				String whoisData0 = (whois0.query(country));
				findWhoisServer.append(whoisData0);
				whois0.disconnect();

			} catch (SocketException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

			String regex2 = "whois:\\s+(.+)";

			// System.out.println("regex2=" + regex2);
			Pattern pattern2 = Pattern.compile(regex2);
			Matcher matcher2 = pattern2.matcher(findWhoisServer.toString());
			if (matcher2.find()) {
				whoisSvr = matcher2.group(1);
			}

			// System.out.println("Whois Server=" + whoisSvr);
		}
		StringBuilder result = new StringBuilder("");

		WhoisClient whois = new WhoisClient();
		try {
			whois.connect(whoisSvr);
			String whoisData1 = whois.query(domain);
			result.append(whoisData1);
			whois.disconnect();

		} catch (SocketException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		JSONObject jsonObj = new JSONObject();
		String response = result.toString();

		int count = 1;
		String[] lines = response.split("\\r?\\n");
		for (String line : lines) {
			// System.out.println("line " + count++ + " : " + line);
			if (line != null && line.contains(">>>")) {
				line = line.replaceAll(">>>\\s*", "");
				line = line.replaceAll("\\s*<<<", "");
			}
			if (line != null && line.contains(":")) {
				// System.out.println(line);
				String[] str = line.split(":\\s+");
				// System.out.println("STR=" + str);
				if (str.length > 1) {
					jsonObj.put(count + "_" + (str[0]).trim(), (str[1]).trim());
				}
			}
			count++;
		}

		// jsonObj.append("response", response);
		jsonObj.put("whois_server", whoisSvr);

		return jsonObj.toString();

	}

}
